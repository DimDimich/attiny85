#!/usr/local/bin/python
import sys

# 1s/(1200000Hz(CPU clock)/1024(configured prescaler)/256(default prescaler)=4.57763671875Hz) = 0.218453s

def clk_ovf_time(p, x=256):
  return 1 / (1e6 / p / x)

print("%.6fs" % clk_ovf_time(*map(float, sys.argv[1:])))

