#ifndef __DATARECEIVER__
#define __DATARECEIVER__

#include <stdlib.h>
#include <stdio.h>

#define WAIT_FOR_DATA 1
#define READING_DATA 2

class DataReceiver {
public:
  uint8_t status;
  uint8_t cursor;
  uint32_t buffer0;
  uint32_t buffer1;
  uint32_t *current_buffer;
  uint32_t *ready_buffer;
  uint8_t buffer_bit_count;

  DataReceiver() {
    buffer_bit_count = sizeof(buffer0) * 8;
    current_buffer = 0;
    swapBuffer();
  }

  void swapBuffer() {
    status = WAIT_FOR_DATA;
    cursor = 0;
    ready_buffer = current_buffer;
    current_buffer = current_buffer == &buffer1 ? &buffer0 : &buffer1;
    *current_buffer = 0;
  }

  void readNextBit(uint8_t bit) {
    *current_buffer |= (bit << cursor++);

    if (cursor == buffer_bit_count) {
      swapBuffer();
    }
  }

  void handleDataPin() {
    switch (status) {
      case WAIT_FOR_DATA:
        status = READING_DATA;
        break;
    }
  }

  void update(uint8_t bit) {
    switch (status) {
      case READING_DATA:
        readNextBit(bit);
        break;
    }
  }

  const uint8_t *popData() {
    uint8_t *b = (uint8_t*)ready_buffer;
    ready_buffer = 0;
    return b;
  }
};

#endif  // __DATARECEIVER__
