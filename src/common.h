#if !defined(__COMMON_H__)
#define __COMMON_H__

#define F_CPU 1000000UL
#include <avr/io.h>
#include <avr/interrupt.h> // работа с прерываниями
#include <avr/wdt.h>       // здесь организована работа с ватчдогом
#include <avr/sleep.h> // здесь описаны режимы сна
#include <util/delay.h>

#endif // __COMMON_H__
