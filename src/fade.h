#include "common.h"
#include "utils.h"

ISR(TIM0_OVF_vect) {
  LED_ON(PB4);
}

ISR(TIM0_COMPA_vect) {
  LED_OFF(PB4);
}

int main() {
  DDRB = 0b00011111;
  TIMSK |= _BV(TOIE0) | _BV(OCIE0A);
  TCCR0A |= _BV(COM0A0);
  TCCR0B |= _BV(CS00) | _BV(CS01);
  OCR0A = 200;

  LED_OFF(PB4);

  sei();

  while (1) {
    asm volatile("nop");
    --OCR0A;
    _delay_ms(10);
  }
}
