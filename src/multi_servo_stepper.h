#include "common.h"
#include "utils.h"

uint8_t servos[] = {1, 1, 1};
uint8_t tc = 0;

ISR(TIM0_COMPA_vect) {
  LED_ON(PB4);
  LED_ON(PB3);
  LED_ON(PB2);

  // 50 tick == 1 sec
  // just increment the values in range from 1 to 8
  if (++tc >= 100) {
    servos[0] = (servos[0] % 8) + 1;
    servos[1] = (servos[1] % 8) + 1;
    servos[2] = (servos[2] % 8) + 1;
    tc = 0;
  }
}

int main() {
  DDRB = 0b00011111;

  TIMSK |= _BV(OCIE0A);
  TCCR1 |= _BV(CTC1);

  TCCR0A |= _BV(COM0A0)| _BV(WGM01);
  TCCR0B |= _BV(CS02);

  // 1 / (1e6 / 256 / 80) = 20ms or 50hz
  OCR0A = 80;

  sei();

  while (1) {
    asm volatile("nop");

    if (TCNT0 == servos[0]) {
      LED_OFF(PB2);
    }

    if (TCNT0 == servos[1]) {
      LED_OFF(PB3);
    }

    if (TCNT0 == servos[2]) {
      LED_OFF(PB4);
    }
  }
}

