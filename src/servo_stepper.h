#include "common.h"
#include "utils.h"

ISR(TIM0_COMPA_vect) {
  LED_ON(PB4);
}

ISR(TIM0_COMPB_vect) {
  LED_OFF(PB4);
}

int main() {
  DDRB = 0b00011111;

  TIMSK |= _BV(OCIE0A) | _BV(OCIE0B);
  TCCR1 |= _BV(CTC1);

  TCCR0A |= _BV(COM0A0) | _BV(COM0B0) | _BV(WGM01);
  TCCR0B |= _BV(CS02);

  // 1 / (1e6 / 256 / 80) = 20ms or 50hz
  OCR0A = 80;

  OCR0B = 1;

  LED_OFF(PB4);

  sei();

  while (1) {
    asm volatile("nop");
    OCR0B = (OCR0B % 8) + 1;
    _delay_ms(1000);
  }
}
