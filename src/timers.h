#include "common.h"
#include "utils.h"

ISR(TIM0_COMPA_vect) {
  LED_TOGGLE(PB4);
}

ISR(TIM1_COMPA_vect) {
  LED_TOGGLE(PB3);
}


int main() {
  // PB3 & PB4 as output
  DDRB |= _BV(PB3) | _BV(PB4);

  // general int mask: allow pin change int
  GIMSK |= _BV(PCIE);

  // enable pin change int on PB0
  PCMSK |= _BV(PB0);
  LED_OFF(PB0);

  // Timer configs
  TIMSK |= _BV(TOIE0) | _BV(OCIE0A) | _BV(OCIE1A);

  //
  TCCR0A |= _BV(COM0A0) | _BV(WGM01);
  TCCR0B |= _BV(CS00) | _BV(CS02);

  TCCR1 |= _BV(CTC1) | _BV(CS10) | _BV(CS11) | _BV(CS12) | _BV(CS13);

  OCR0A = 255;
  OCR1C = 255;

  sei();

  while (1) {
    asm volatile("nop");
  }
}

