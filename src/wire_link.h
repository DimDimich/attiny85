#include "common.h"
#include "utils.h"
#include "DataReceiver.h"

DataReceiver dr;
const uint8_t *data;

ISR(TIM1_COMPA_vect) {
  dr.update(BTN_PRESSED(PB0));
  if (data = dr.popData()) {
    if (data[0] == 0b01101001 && data[1] == 0b01100011 && data[2] == 0b01110111 && data[3] == 0b01111011) {
      LED_TOGGLE(PB3);
    }
  }
}

ISR(PCINT0_vect) {
  if (BTN_PRESSED(PB0)) {
    dr.handleDataPin();
  }
}

int main() {
  // PB3 & PB4 as output
  DDRB |= _BV(PB3) | _BV(PB4);

  // general int mask: allow pin change int
  GIMSK |= _BV(PCIE);

  // enable pin change int on PB0
  PCMSK |= _BV(PB0);

  LED_OFF(PB0);

  // Timer configs
  TIMSK |= _BV(TOIE0) | _BV(OCIE1A);

  TCCR1 |= _BV(CTC1) | _BV(CS10) | _BV(CS11);

  OCR1C = 250;

  sei();

  while (1) {
    asm volatile("nop");
  }
}

